module gitlab.com/syifan/l15

go 1.13

require (
	github.com/sclevine/agouti v3.0.0+incompatible // indirect
	github.com/tebeka/atexit v0.3.0
	gitlab.com/akita/akita v1.10.1
	gitlab.com/akita/mem v1.8.6
	gitlab.com/akita/mgpusim v1.8.2-0.20200714182616-9db3500d293b
	gitlab.com/akita/noc v1.4.0
	gitlab.com/akita/util v0.6.4
)
